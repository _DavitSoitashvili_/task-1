<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Calculate</title>
</head>
<body>
<h1 style="text-align: center">{{$firstNumber}} {{$action}} {{$secondNumber}} = {{$result}}</h1>
</body>
</html>
