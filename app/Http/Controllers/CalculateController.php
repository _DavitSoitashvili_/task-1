<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class CalculateController extends Controller
{
    function calculate(Request $request)
    {
        $firstNumber = $request->get("numberOne");
        $secondNumber = $request->get("numberTwo");
        $action = $request->get("action");

        switch ($action) {
            case "sum":
                $result = $firstNumber + $secondNumber;
                return View("calculate", ["firstNumber" => $firstNumber, "secondNumber" => $secondNumber, "action" => "+", "result" => $result]);
            case "subtraction":
                $result = $firstNumber - $secondNumber;
                return View("calculate", ["firstNumber" => $firstNumber, "secondNumber" => $secondNumber, "action" => "-", "result" => $result]);
            case "multiply":
                $result = $firstNumber * $secondNumber;
                return View("calculate", ["firstNumber" => $firstNumber, "secondNumber" => $secondNumber, "action" => "*", "result" => $result]);
            case "divide":
                $result = $firstNumber / $secondNumber;
                return View("calculate", ["firstNumber" => $firstNumber, "secondNumber" => $secondNumber, "action" => "/", "result" => $result]);
            default:
                return View("error");

        }

    }

}
